#include "../header/possible_char.hpp"
#include "../header/tools.hpp"

#include <iostream>
#include <string>


int main(int argc, char* args[]) {
    std::string s1 = generate_random_string(DNA_CHARS, 100);
    std::string s2 = generate_random_string(DNA_CHARS, 100);

    std::cout << "s1: " << s1 << std::endl;
    std::cout << "s2: " << s2 << std::endl;

    std::string lcs = longest_common_subsequence(s1, s2);

    std::cout << "lcs: " << lcs << std::endl;

    return EXIT_SUCCESS;
}