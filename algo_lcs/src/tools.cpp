#include <algorithm>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>


/**
 * Finds the longest common subsequence between two strings.
 * 
 * @param s1 The first string.
 * @param s2 The second string.
 * 
 * @return The longest common subsequence as a string.
 */
std::string longest_common_subsequence(std::string s1, std::string s2) {
    const int len_s1 = s1.length();
    const int len_s2 = s2.length();

    std::vector<std::vector<int>> A(len_s1 + 1, std::vector<int>(len_s2 + 1));
    
    for(int i = 0; i < len_s1; i++) {
        for(int j = 0; j < len_s2; j++) {
            if(s1[i] == s2[j]) {
                A[i + 1][j + 1] = A[i][j] + 1;
            } else {
                A[i + 1][j + 1] = std::max(A[i][j + 1], A[i + 1][j]);
            }
        }
    }

    std::vector<char> result;
    int i = len_s1;
    int j = len_s2;

    while(A[i][j] > 0) {
        if(A[i][j] == A[i - 1][j]) {
            i--;
        } else if(A[i][j] == A[i][j - 1]) {
            j--;
        } else {
            result.push_back(s1[i - 1]);
            i--;
            j--;
        }
    }

    std::reverse(result.begin(), result.end());
    return std::string(result.begin(), result.end());
}


/**
 * Generates a random string of the specified size using the given set of possible characters.
 * 
 * @param possible_chars The set of possible characters to choose from.
 * @param size The size of the generated string.
 * 
 * @return The randomly generated string.
 */
std::string generate_random_string(std::vector<std::string> possible_chars, int size){
    std::string s;
    for(int i = 0; i < size; i++) {
        s += possible_chars[rand() % possible_chars.size()];
    }
    return s;
}


/**
 * Reads the content of a file and returns it as a string.
 * 
 * @param filename The name of the file to read.
 * 
 * @return The content of the file as a string.
 */
std::string read_from_file(std::string filename) {
    std::ifstream file(filename);
    std::string content;

    if (file) {
        std::stringstream buffer;
        buffer << file.rdbuf();
        content = buffer.str();
        file.close();
    }

    return content;
}
