#include <string>
#include <vector>


std::vector<std::string> ALPHABETIC_CHARS = {"a", "b", "c", "d", "e", "f", "g", "h", "i",
                                         "j", "k", "l", "m", "n", "o", "p", "q", "r",
                                         "s", "t", "u", "v", "w", "x", "y", "z"};


std::vector<std::string> NUMERIC_CHARS = {"1", "2", "3","4","5","6","7","8","9","0"};

std::vector<std::string> DNA_CHARS = {"A", "C", "G", "T"};