#ifndef TOOLS_HPP
#define TOOLS_HPP

#include <string>
#include <vector>

std::string longest_common_subsequence(std::string, std::string);
std::string generate_random_string(std::vector<std::string> , int);
std::string read_from_file(std::string);

#endif