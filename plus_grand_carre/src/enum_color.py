"""
Enum Color
"""

from enum import Enum


class Colors(Enum):
    """All possible colors"""
    BLACK = 1
    WHITE = 0
