from enum_color import Colors
from generate_grid import generate_random_grid, generate_random_grid_with_percentage
from display_utils import display_grid_png, display_grid_terminal, display_rectangle, display_in_file, get_grid_from_file
from rectangle_calculation import biggest_rectangle_from_grid
from typing import List
import drawsvg as draw


def main() -> None:
    # grid: List[List[Colors]] = generate_random_grid(100)
    grid: List[List[Colors]] = generate_random_grid_with_percentage(20, 0.8)
    # grid: List[List[Colors]] = get_grid_from_file("grid_clear.txt")
    display_in_file(grid, "grid_clear.txt")
    drawing: draw.Drawing = display_grid_png(grid)
    # display_grid_terminal(grid)

    area, left, height, right, bottom = biggest_rectangle_from_grid(grid)

    display_rectangle(drawing, left, height, right, bottom)

    print(f"Aire du plus grand rectangle noir: {area}")


if __name__ == "__main__":
    main()
