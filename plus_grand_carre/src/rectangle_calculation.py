"""
Rectangle calculation
"""

from typing import List, Tuple
from enum_color import Colors


def biggest_rectangle_from_histogram(H: List[float]) -> Tuple[float, float, float, float]:
    """Biggest rectangle from histogram

    Args:
        H (List[float]): Histogram

    Returns:
        Tuple[float, float, float, float]: Biggest rectangle from histogram (area, left, height, right)"""
    best = (float('-inf'), 0.0, 0.0, 0.0)
    S = []
    H2 = H + [float('-inf')]
    for right in range(len(H2)):
        x = H2[right]
        left = right
        while len(S) > 0 and S[-1][1] >= x:
            left, height = S.pop()
            rect = (height * (right - left), left, height, right)
            if rect > best:
                best = rect
        S.append((left, x))
    return best


def biggest_rectangle_from_grid(P: List[List[Colors]]) -> Tuple[float, float, float, float, float]:
    """Biggest rectangle from grid

    Args:
        P (List[List[float]]): Grid

    Returns:
        Tuple[float, float, float, float, float]: Biggest rectangle from grid (area, left, height, right, bottom)
    """
    rows = len(P)
    cols = len(P[0])
    t = [0] * cols
    best = None
    for i in range(rows):
        for j in range(cols):
            if P[i][j] == Colors.BLACK:
                t[j] += 1
            else:
                t[j] = 0
        (area, left, height, right) = biggest_rectangle_from_histogram(t)
        alt = (area, left, i, right, i - height)
        if best is None or alt > best:
            best = alt
    return best
