"""
Grid Generation
"""

import random
from enum_color import Colors
from typing import List


def generate_random_grid(size: int) -> List[List[Colors]]:
    """Generate random grid

    Args:
        size (int): Size

    Returns:
        List[List[Colors]]: Grid"""

    return [[random.choice(list(Colors)) for _ in range(size)] for _ in range(size)]


def generate_random_grid_with_percentage(size: int, percentage: float) -> List[List[Colors]]:
    """Generate random grid with percentage

    Args:
        size (int): Size
        percentage (float): Percentage

    Returns:
        List[List[Colors]]: Grid"""

    return [[Colors.BLACK if random.random() < percentage else Colors.WHITE for _ in range(size)] for _ in range(size)]



