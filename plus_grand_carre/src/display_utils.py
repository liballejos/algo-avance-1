from enum_color import Colors
import drawsvg as draw
from typing import List, Dict
import os

HEXACOLORS_PER_COLOR: Dict[Colors, str] = {
    Colors.BLACK: "#000000",
    Colors.WHITE: "#FFFFFF"
}


def display_grid_png(grid: List[List[Colors]], file_name: str = 'grid_clear.png') -> draw.Drawing:
    """Display grid

    Args:
        grid (List[List[Colors]]): Grid
        file_name (str, optional): File name. Defaults to 'grid_clear.png'.

    Returns:
        draw.Drawing: Drawing"""

    drawing: draw.Drawing = draw.Drawing(len(grid[0]) * 100, len(grid) * 100, origin=(0, 0))

    for i in range(len(grid)):
        for j in range(len(grid[i])):
            drawing.append(draw.Rectangle(j * 100, i * 100, 100, 100,
                                          fill=HEXACOLORS_PER_COLOR[grid[i][j]]))
    drawing.save_png(file_name)
    return drawing


def display_grid_terminal(grid) -> None:
    """Display grid

    Args:
        grid (List[List[Colors]]): Grid"""
    for i in range(len(grid)):
        for j in range(len(grid[i])):
            print(grid[i][j].value, end=' ')
        print()


def display_rectangle(drawing: draw.Drawing,
                      left: float,
                      height: float,
                      right: float,
                      bottom: float,
                      file_name: str = "grid_answer.png") -> None:
    """
    Display rectangle

    Args:
        drawing (draw.Drawing): Drawing
        left (float): Left
        height (float): Height
        right (float): Right
        bottom (float): Bottom
        file_name (str, optional): File name. Defaults to "grid_answer.png".

    Returns:
        None
    """

    drawing.append(draw.Rectangle(left * 100,
                                  (bottom + 1) * 100,
                                  (right - left) * 100,
                                  (height - bottom) * 100,
                                  fill="#FF0000"))
    drawing.save_png(file_name)


def display_in_file(grid: List[List[Colors]],
                    file_name: str = 'grid.txt') -> None:
    """Display grid

    Args:
        grid (List[List[Colors]]): Grid
        file_name (str, optional): File name. Defaults to 'grid_clear.txt'.

    Returns:
        None"""

    current_path: str = os.path.abspath(__file__)
    path_to_file: str = os.path.join(os.path.dirname(current_path), f"../data/{file_name}")

    with open(path_to_file, "w") as file:
        file.write(f"{len(grid)}\n")
        for i in range(len(grid)):
            for j in range(len(grid[i])):
                file.write(f"{str(grid[i][j].value)} ")
            file.write("\n")


def get_grid_from_file(file_name: str = 'grid.txt') -> List[List[Colors]]:
    """Get grid from file

    Args:
        file_name (str, optional): File name. Defaults to 'grid.txt'.

    Returns:
        List[List[Colors]]: Grid"""

    current_path: str = os.path.abspath(__file__)
    path_to_file: str = os.path.join(os.path.dirname(current_path), f"../data/{file_name}")

    with open(path_to_file, "r") as file:
        lines: List[str] = file.readlines()
        size: int = int(lines[0])
        grid: List[List[Colors]] = [[Colors(int(lines[i].split()[j])) for j in range(size)] for i in range(1, size + 1)]
        return grid
