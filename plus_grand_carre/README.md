# Plus grand carré

## Setup venv

```bash
python3 -m venv venv
source venv/bin/activate
pip install -r requirements.txt
```

## How to run

```bash
python3 src/main.py
```

## How to test

```bash
pytest test/
```