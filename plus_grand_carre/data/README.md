# data folder

## Usage

You can store and read data from this folder.

## File disposition example

```txt
10
0 0 0 1 0 1 1 0 0 1 
1 0 0 0 1 1 1 0 0 0 
1 1 0 1 0 1 0 1 0 0 
0 1 0 0 1 1 1 0 0 0 
1 1 0 0 0 1 1 1 0 1 
1 0 1 1 0 0 0 1 1 0 
0 0 1 0 0 1 1 1 1 0 
1 0 1 0 1 1 1 1 1 0 
1 0 1 1 0 0 1 0 1 0 
1 0 1 0 1 0 1 1 1 0 
```


